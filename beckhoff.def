##################
#Type: Neutron Chopper Drive
#Project: Chopper Integration Controller (CHIC)

##################
# STATUS BLOCK
##################
define_status_block()

add_major_alarm("HW_Alrm", "Hardware Alarm", PV_DESC="Hardware alarms active in drive")
add_analog("nHW_Alrm", "INT", PV_DESC="Bit mapping for Hardware Alarm")
add_major_alarm("SW_Alrm", "Software Alarm", PV_DESC="Software alarms active in drive")
add_analog("nSW_Alrm", "INT", PV_DESC="Bit mapping for Software Alarm")
add_major_alarm("V_Alrm", "Voltage Alarm", PV_DESC="Voltage alarms active in drive")
add_analog("nV_Alrm", "INT", PV_DESC="Bit mapping for Voltage Alarm")
add_major_alarm("Pos_Alrm", "Position Alarm", PV_DESC="Invalid position or rotation alarms")
add_analog("nPos_Alrm", "INT", PV_DESC="Bit mapping for Position Alarm")
add_major_alarm("Tmp_Alrm", "Temperature Alarm", PV_DESC="Temperature alarms active")
add_analog("nTmp_Alrm", "INT", PV_DESC="Bit mapping for Temperature Alarm")
add_major_alarm("ILck_Alrm", "Interlock Alarm", PV_DESC="Interlock alarms active")
add_analog("nILck_Alrm", "INT", PV_DESC="Bit mapping for Interlock Alarm")
add_major_alarm("I_Alrm", "Current Alarm", PV_DESC="Electrical current alarms active")
add_analog("nI_Alrm", "DINT", PV_DESC="Bit mapping for Current Alarm")
add_major_alarm("P_Alrm", "Energy Alarm", PV_DESC="Electrical power alarms active. Watts")
add_analog("nP_Alrm", "INT", PV_DESC="Bit mapping for Energy Alarm")
add_major_alarm("Comm_Alrm", "Communications Alarm", PV_DESC="Communication alarm active")
add_analog("nComm_Alrm", "INT", PV_DESC="Bit mapping for Communications Alarm")
add_major_alarm("Ref_Alrm", "Reference signal Alarm", PV_DESC="Reference/Synchronization signal alarm")
add_enum("Lvl_Alrm", "INT", PV_DESC="Level of Alarms by enumeration", PV_ZRST="0=No Alarms", PV_ONST="1=Alarms", PV_ONSV="MINOR", PV_TWST="2=Trip", PV_TWSV="MAJOR")
add_enum("Chop_Stat", "INT", PV_DESC="Chopper State by enumeration", PV_ZRST="0=Comms not ok", PV_ONST="1=Initialization", PV_TWST="2=Ready", PV_THST="3=Rotating", PV_FRST="4=Coasting", PV_FVST="5=Stopping", PV_SXST="6=E. stopping", PV_SXSV="MAJOR")
add_enum("Lev_Stat", "INT", PV_DESC="Levitation status", PV_ZRST="0= Not levitated", PV_ONST="1=Levitating", PV_TWST="2=Levitated", PV_THST="3=Delevitating")
add_enum("Park_Stat", "INT", PV_DESC="Parking status ", PV_ZRST="0=Not Parked", PV_ONST="1=Parked", PV_TWST="2=Parking")
add_enum("InPhs_Stat", "INT", PV_DESC="Phase status", PV_ZRST="0=Not in Phase", PV_ONST="1=In Phase")
add_enum("OnSpd_Stat", "INT", PV_DESC="Speed status", PV_ZRST="0=Not on speed", PV_ONST="1=On speed")
add_enum("Vac_Stat", "INT", PV_DESC="Vacuum status", PV_ZRST="0=Not ok", PV_ONST="1=Ok")
add_enum("RotMd_Stat", "INT", PV_DESC="Rotation mode", PV_ZRST="Dir.0=CW,1=CCW", PV_ONST="Mode.0=Spd,1=Ph", PV_TWST="Mtr pos.0=w/beam,1=a/beam")
add_analog("SpdSP_Stat", "REAL", PV_DESC="Readback. Rot speed setpoint in Hz", PV_EGU="Hz")
add_analog("Spd_Stat", "REAL", PV_DESC="Rotational speed in Hz", PV_EGU="Hz")
add_analog("MtrTmp_Stat", "REAL", PV_DESC="Motor temperature in Celsius degrees", PV_EGU="C")
add_analog("DrvTmp_Stat", "REAL", PV_DESC="Motor drive temperature in Celsius ", PV_EGU="C")
add_analog("Pos_Stat", "REAL", PV_DESC="Postion of the rotor. 0-359deg", PV_EGU="Deg")
add_analog("TimOn_Stat", "REAL", PV_DESC="Secs the controller has been powered on", PV_EGU="S")
add_analog("TimLevi_Stat", "REAL", PV_DESC="Secs the rotor has been levitated", PV_EGU="S")
add_analog("TimRot_Stat", "REAL", PV_DESC="Secs the rotor has been rotating", PV_EGU="S")
add_analog("ParkSPPos1_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP1")
add_analog("ParkSPPos2_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP2")
add_analog("ParkSPPos3_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP3")
add_analog("ParkSPPos4_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP4")
add_analog("ParkSPPos5_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP5")
add_analog("ParkSPPos6_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP6")
add_analog("ParkSPPos7_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP7")
add_analog("ParkSPPos8_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP8")
add_analog("ParkSPPos9_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP9")
add_analog("ParkSPPos10_Stat", "REAL", PV_DESC="RBV.ParkSP saved on ParkPos_SP10")
add_analog("PosV13_Stat", "REAL", PV_DESC="Peak position V13.", PV_EGU="μm")
add_analog("PosW13_Stat", "REAL", PV_DESC="Peak position W13. μm", PV_EGU="μm")
add_analog("PosV24_Stat", "REAL", PV_DESC="Peak position V24. μm", PV_EGU="μm")
add_analog("PosW24_Stat", "REAL", PV_DESC="Peak position W24. μm", PV_EGU="μm")
add_analog("PosZ12_Stat", "REAL", PV_DESC="Peak position Z12.μm", PV_EGU="μm")
add_analog("MaxSpd_Stat", "REAL", PV_DESC="Maximum speed of the system ", PV_EGU="Hz")
add_analog("MinSpd_Stat", "REAL", PV_DESC="Minimum speed of the system ", PV_EGU="Hz")
add_enum("CtrlMd_Stat", "INT", PV_DESC="Control Mode", PV_ZRST="0=local", PV_ONST="1=chic", PV_TWST="2=epics")

##################
# COMMAND BLOCK
##################
define_command_block()

add_enum("Cmd", "INT", PV_DESC="Commands by enumeration.", PV_ZRST="null", PV_ONST="reset", PV_TWST="calibrate", PV_THST="stop",  PV_FRST="estop",  PV_FVST="astart",  PV_SXST="start",  PV_SVST="coast",  PV_EIST="clear")

##################
# PARAMETER BLOCK
##################
define_parameter_block()

add_analog("ParkPos_SP", "INT", PV_DESC="1=close, 2=open1, 3=open2,…")
add_analog("Park_SP", "REAL", PV_DESC="ParkSP 0-359deg saved on ParkPos_SPx", PV_EGU="Deg")
add_analog("Spd_SP", "REAL", PV_DESC="Rotation speed setpoint in Hz. Neg=CCW", PV_EGU="Hz")
add_enum("CtrlMd_SP", "INT", PV_DESC="Control Mode", PV_ZRST="local", PV_ONST="chic", PV_TWST="epics")
